package com.example.service;



import com.example.pojo.Resume;

import java.util.List;

public interface IResumeService {

    List<Resume> findAll();

    void delete(Resume resume);

    void update(Resume resume);

    void insert(Resume resume);
}
