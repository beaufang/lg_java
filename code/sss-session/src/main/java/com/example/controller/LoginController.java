package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.vo.ResponseMsg;
import com.example.vo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class LoginController {

    @RequestMapping("/")
    public String index() {
        return "index.html";
    }


    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseMsg login(@RequestBody User user, HttpSession session, Model model) {
        System.out.println("========== do login ===========");
        // 登录成功
        if (user != null && "admin".equals(user.getUsername()) && "admin".equals(user.getUsername())) {
            session.setAttribute("login", JSON.toJSONString(user));
            session.setMaxInactiveInterval(3000);
            return new ResponseMsg("ok", 200);
        }
        return new ResponseMsg("Username or Password May be Wrong", 401);
    }

}
