package com.example.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author BeauFang
 * Date: 2020/8/12
 */
@Configuration
public class RabbitConfig {

    @Bean
    public Queue orderQueue() {
        Map<String, Object> props = new HashMap<>();
        // 设置消息过期时间 10s
        props.put("x-message-ttl", 10000);
        // 设置该队列所关联的死信交换器（当队列消息TTL到期后依然没有消费，则加入死信 队列）
        props.put("x-dead-letter-exchange", "ex.order.dlx");
        // 设置该队列所关联的死信交换器的routingKey，如果没有特殊指定，使用原队列的 routingKey
        props.put("x-dead-letter-routing-key", "order.dlx");
        return new Queue("q.order", true, false, false, props);
    }

    @Bean
    // 死信队列
    public Queue orderQueueDlx() {
        return new Queue("q.order.dlx", true, false, false);
    }

    @Bean
    public Exchange exchange() {
        return new DirectExchange("ex.order", true, false, null);
    }

    // 死信交换器
    @Bean
    public Exchange exchangeDlx() {
        return new DirectExchange("ex.order.dlx", true, false, null);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(orderQueue()).to(exchange()).with("order").noargs();
    }

    // 死信交换器绑定死信队列
    @Bean
    public Binding bindingDlx() {
        return BindingBuilder.bind(orderQueueDlx()).to(exchangeDlx()).with("order.dlx").noargs();
    }
}
