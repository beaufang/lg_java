package com.example.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author BeauFang
 * Date: 2020/8/12
 */
@Data
@Entity
@Table(name = "goods")
public class Goods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String name;

    private String goodsId;
}
