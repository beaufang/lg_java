package com.example.consumer;

import com.example.pojo.Order;
import com.example.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
@Component
public class OrderDlxConsumer {

    @Autowired
    private OrderService orderService;

    @RabbitListener(queues = "q.order.dlx")
    public void onOrderMsg(Order msg) {
        System.out.println("收到来自死信队列的订单消息");
        Order order = orderService.findById(msg.getId());
        if (order != null && order.getState() == Order.STATE.UNPAY) {
            order.setState(Order.STATE.CANCLE);
            orderService.save(order);
            System.out.println("订单支付超时，已经取消");
        }
    }
}
