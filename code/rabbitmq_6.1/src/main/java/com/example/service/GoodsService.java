package com.example.service;

import com.example.pojo.Goods;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/12
 */
public interface GoodsService {

    List<Goods> findAll();
}
