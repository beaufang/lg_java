package com.example.service.impl;

import com.example.dao.OrderDao;
import com.example.pojo.Order;
import com.example.service.OrderService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private OrderDao orderDao;

    /**
     * 将订单保存到数据库，并发送到 mq
     * @param order
     */
    @Override
    public void submit(Order order) {
        // ex.order exchange 名称
        // ”order“ routingKey
        // order 对象，发送的 msg
        orderDao.save(order);
        amqpTemplate.convertAndSend("ex.order","order", order);
    }

    @Override
    public List<Order> findAll() {
        return orderDao.findAll();
    }

    @Override
    public Order findById(String id) {
        return orderDao.findById(id).get();
    }

    @Override
    public void save(Order order) {
        orderDao.save(order);
    }


}
