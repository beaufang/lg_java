package com.example.service;

import com.example.pojo.Order;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
public interface OrderService {
    void submit(Order order);
    List<Order> findAll();
    Order findById(String id);
    void save(Order order);
}
