package com.example.dao;

import com.example.pojo.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author BeauFang
 * Date: 2020/8/12
 */
public interface GoodsDao extends JpaRepository<Goods, String> {
}
