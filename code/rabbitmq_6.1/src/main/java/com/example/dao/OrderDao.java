package com.example.dao;

import com.example.pojo.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
public interface OrderDao  extends JpaRepository<Order, String> {

}
