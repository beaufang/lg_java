package com.example.demo.consumer;

import com.example.demo.pojo.Order;
import com.example.demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author BeauFang
 * Date: 2020/9/10
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "seckill_phone_topic",consumerGroup = "seckill-consumer-group-1")
public class MQOrderListener implements RocketMQListener<Order> {

    @Autowired
    private OrderService orderService;

    @Override
    public void onMessage(Order order) {
        // 保存订单信息
        orderService.save(order);
        log.info("保存订单信息");
    }
}
