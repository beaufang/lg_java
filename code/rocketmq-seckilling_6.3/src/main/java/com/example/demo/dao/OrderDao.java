package com.example.demo.dao;


import com.example.demo.pojo.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
public interface OrderDao  extends JpaRepository<Order, String> {

    @Transactional
    @Modifying
    @Query("update Order set state = 2 where state = 0 and createTime < ?1")
    int updateOrderState(long time);
}
