package com.example.demo.config;

import com.example.demo.util.SnowFlake;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author BeauFang
 * Date: 2020/9/12
 */
@Configuration
public class MyConfiguration {

    @Bean
    public SnowFlake snowFlake() {
        return new SnowFlake(1L, 1L);
    }
}
