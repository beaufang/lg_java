package com.example.demo.service;



import com.example.demo.pojo.Order;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/13
 */
public interface OrderService {
    boolean checkInventory();
    void submit(Order order);
    List<Order> findAll();
    Order findById(String id);
    void save(Order order);
    int updateOrderState(long time);

    void initInventory();
}
