package com.example.demo.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author BeauFang
 * Date: 2020/8/12
 */
@Data
@Entity
@Table(name = "orders")
public class Order implements Serializable {
    @Id
//    @GeneratedValue(generator="system-uuid")
//    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
    private String goodsId;
    private Long createTime;
    // 0 未支付、1 已支付、2 已取消
    private Integer state;

    public interface STATE {
        int UNPAY = 0;
        int FINISHED = 1;
        int CANCLE = 2;
    }
}
