package com.beau.service.impl;

import com.beau.dao.ResumeDao;
import com.beau.pojo.Resume;
import com.beau.service.IResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ResumeService implements IResumeService {
    @Autowired
    private ResumeDao resumeDao;

    @Override
    public List<Resume> findAll() {
        return resumeDao.findAll();
    }

    @Override
    public void delete(Resume resume) {
        resumeDao.delete(resume);
    }

    @Override
    public void update(Resume resume) {
        resumeDao.save(resume);
    }

    @Override
    public void insert(Resume resume) {
        resumeDao.save(resume);
    }

}
