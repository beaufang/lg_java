package com.beau.service;

import com.beau.pojo.Resume;

import java.util.List;

public interface IResumeService {

    List<Resume> findAll();

    void delete(Resume resume);

    void update(Resume resume);

    void insert(Resume resume);
}
