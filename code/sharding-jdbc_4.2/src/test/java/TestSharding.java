import com.beau.SpringApp;
import com.beau.dao.OrderDao;
import com.beau.pojo.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringApp.class)
public class TestSharding {

    @Resource
    private OrderDao orderDao;

    @Test
    public void writeTest() {
        Random random = new Random(10000);
        for (int i =0; i<8; i++) {
            Order order = new Order();
            order.setIsDel(false);
            order.setCompanyId(100000 + i);
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            order.setPublishUserId(20000 + i);
            order.setPositionId(30000 + i);
            order.setPublishUserId(40000 + i);
            order.setResumeType(i%2);
            order.setStatus("WAIT");
            order.setUserID(Math.abs(random.nextInt()));
            orderDao.save(order);
        }

    }

    @Test
    public void readTest() {
        List<Order> all = orderDao.findAll();
        for (Order o: all) {
            System.out.println(o);
        }
    }
}
