package com.example.blog.controller;

import com.example.blog.pojo.Article;
import com.example.blog.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private IArticleService articleService;

    @RequestMapping("/")
    public String getIndexPage(@RequestParam(defaultValue = "0") int pageNum,
                               @RequestParam(defaultValue = "2") int size, Model model) {
        Page<Article> articles = articleService.getArticle(pageNum, size);
        model.addAttribute("articles", articles);
        return "/client/index";
    }

}
