package com.example.blog.service;

import com.example.blog.pojo.Article;
import org.springframework.data.domain.Page;

public interface IArticleService {

    Page<Article> getArticle(int startPage, int size);

}
