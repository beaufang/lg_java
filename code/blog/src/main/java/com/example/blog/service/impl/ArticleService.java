package com.example.blog.service.impl;

import com.example.blog.pojo.Article;
import com.example.blog.repository.ArticleRepository;
import com.example.blog.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ArticleService implements IArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    @Cacheable(cacheNames = "articles")
    public Page<Article> getArticle(int startPage, int size) {
        return articleRepository.findAll(PageRequest.of(startPage, size));
    }

}
