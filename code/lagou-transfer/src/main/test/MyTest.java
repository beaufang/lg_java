import com.lagou.edu.factory.BeanFactory;
import com.lagou.edu.factory.ProxyFactory;
import com.lagou.edu.service.TransferService;
import com.lagou.edu.service.impl.TransferServiceImpl;
import org.junit.Test;

public class MyTest {

    @Test
    public void test01() {
        TransferService transferService = (TransferService) BeanFactory.getBean("transferService");
        System.out.println(transferService);
    }

    @Test
    public void test02() {
        ProxyFactory factory = new ProxyFactory();
        TransferService t = new TransferServiceImpl();
        Object jdkProxy1 = factory.getJdkProxy(t);
        Object jdkProxy2 = factory.getJdkProxy(t);
        System.out.println(jdkProxy1 == jdkProxy2);

    }
}
