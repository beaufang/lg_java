import com.beau.dao.IUserDao;
import com.beau.io.Resources;
import com.beau.pojo.User;
import com.beau.sqlSession.SqlSession;
import com.beau.sqlSession.SqlSessionFactory;
import com.beau.sqlSession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {

    private IUserDao mapper;

    @Before
    public void init() throws PropertyVetoException, DocumentException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(inputStream);
        SqlSession sqlSession = factory.openSession();
        mapper = sqlSession.getMapper(IUserDao.class);
    }

    @Test
    public void testFindAll() throws Exception {
        List<User> list = mapper.findAll();
        for (User u : list) {
            System.out.println(u);
        }
    }

    @Test
    public void testFindByCondition() throws Exception {
        User user = new User();
        user.setId(1);
        user.setUsername("zhangsan");
        User user1 = mapper.findByCondition(user);
        System.out.println(user1);
    }

    @Test
    public void testUpdateUserById() throws Exception {
        User user = new User();
        user.setId(1);
        user.setUsername("tom");
        int r = mapper.updateUserById(user);
        System.out.println(r);
    }

    @Test
    public void testDeleteUserByName() throws Exception {
        User user = new User();
        user.setUsername("tom");
        int r = mapper.deleteUserByName(user);
        System.out.println(r);
    }

    @Test
    public void testInsertUser() throws Exception {
        User user = new User();
        user.setUsername("tom");
        user.setPassword("123456");
        int r = mapper.insertUser(user);
        System.out.println(r);
    }
}
