package com.example;

import com.example.handler.RpcHandler;
import com.example.server.RpcServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RpcServerApplication {
	@Value("${rpc.port}")
	private int port;
	@Value("${rpc.host}")
	private String host;

	public static void main(String[] args) {
		SpringApplication.run(RpcServerApplication.class, args);
	}

	@Bean(initMethod = "start")
	public RpcServer rpcServer() {
		return new RpcServer(host, port);
	}


}
