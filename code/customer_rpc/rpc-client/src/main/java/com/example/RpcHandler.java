package com.example;

import com.example.request.RpcRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.Callable;

public class RpcHandler extends SimpleChannelInboundHandler<String> implements Callable {

    private ChannelHandlerContext context;
    private RpcRequest request;
    private String result;



    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        this.context = ctx;
    }

    @Override
    public synchronized Object call() throws Exception {
        context.writeAndFlush(request); // 发送请求
        System.out.println("send request");
        wait(); // 等待结果返回
        return result;
    }

    @Override
    protected synchronized void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        result = msg;
        notifyAll();
    }

    public void setRequest(RpcRequest request) {
        this.request = request;
    }
}
