package com.example.demo.dao;

import com.example.demo.pojo.Resume;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ResumeDao extends MongoRepository<Resume, String> {
}
