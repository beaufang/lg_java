package com.example.demo;

import com.example.demo.dao.ResumeDao;
import com.example.demo.pojo.Resume;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        ResumeDao resumeDao = context.getBean(ResumeDao.class);
        for (int i = 0; i < 1000; i++) {
            Resume resume  = new Resume();
            resume.setName("test" + i);
            resume.setExpectSalary(10000 + i);
            resume.setCity("武汉");
            resumeDao.save(resume);
        }
    }

}
