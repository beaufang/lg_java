package com.beau.service;

public interface UserService {

    boolean isRegistered(String email);

    String findEmail(String token);

    void saveToken(String email, String token);

    Boolean validate(String email, String passwd);

    String info(String token);

}
