package com.beau.controller;


import com.beau.service.CodeService;
import com.beau.service.UserService;
import com.beau.util.TokenUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("user")
public class UserController {


    @Reference
    private UserService userService;

    @Reference
    private CodeService codeService;


    /**
     * 注册成功返回 0，验证码错误返回 1，验证码超时返回 2
     * @param email
     * @param password
     * @param code
     * @return
     */
    @GetMapping("/register/{email}/{password}/{code}")
    public Integer register(@PathVariable("email") String email,
                            @PathVariable("password") String password, @PathVariable("code") String code,
                            HttpServletResponse response) {

        // 验证邮箱和验证码,0正确1错误2超时
        Integer repCode = codeService.validateCode(email, code);
        if (repCode != 0) {
            return repCode;
        }
        // 根据邮箱和密码生成 token，写入数据库
        String token = TokenUtil.genToken(email + ":" + password);
        // token 写入数据库
        userService.saveToken(email, token);
        // token 写入 cookie
        Cookie tokenCookie = new Cookie("token", token);
        // path 标识指定了主机下的哪些路径可以接受Cookie，如果不设置，默认是 url 全路及其子路径才可以访问到该 cookie
        tokenCookie.setPath("/");
        response.addCookie(tokenCookie);
        return 0;
    }


    @GetMapping("/isRegistered/{email}")
    public Boolean isRegistered(@PathVariable("email") String email) {
        return userService.isRegistered(email);
    }

    /**
     * 登录成功返回邮箱地址，重定向到欢迎页
     * 失败，返回 null
     * @param email
     * @param password
     * @return
     */
    @GetMapping("/login/{email}/{password}")
    public String login(@PathVariable("email") String email, @PathVariable("password") String password,
                        HttpServletResponse response) {
        // 登录成功，返回邮箱地址
        if (userService.validate(email, password)) {
            // token 写入 cookie
            String token = TokenUtil.genToken(email + ":" + password);
            Cookie tokenCookie = new Cookie("token", token);
            // path 标识指定了主机下的哪些路径可以接受Cookie，如果不设置，默认是 url 全路及其子路径才可以访问到该 cookie
            tokenCookie.setPath("/");
            response.addCookie(tokenCookie);
            return email;
        }
        return "";
    }

    /**
     * 根据 token 查询用户邮箱
     * @param token
     * @return
     */
    @GetMapping("/info/{token}")
    public String info(@PathVariable("token") String token) {
        return userService.findEmail(token);
    }
}
