package com.beau.dao;

import com.beau.common.pojo.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TokenDao extends JpaRepository<Token, Integer> {

    @Query("from Token where email = ?1")
    Token findByEmail(String email);

    @Query("from Token where token = ?1")
    Token findByToken(String token);
}
