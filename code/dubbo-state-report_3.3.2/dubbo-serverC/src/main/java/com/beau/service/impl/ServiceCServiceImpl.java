package com.beau.service.impl;

import com.beau.service.ServercService;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

@Service
public class ServiceCServiceImpl implements ServercService {

    private Logger Log = LoggerFactory.getLogger(ServiceCServiceImpl.class);
    private final Random random = new Random();

    @Override
    public String methodA() throws InterruptedException {
        int sleepTime = random.nextInt(100);
        Log.info("sleep {}ms", sleepTime);
        Thread.sleep(sleepTime);
        return "call methodA";
    }

    @Override
    public String methodB() throws InterruptedException {
        int sleepTime = random.nextInt(100);
        Log.info("sleep {}ms", sleepTime);
        Thread.sleep(sleepTime);
        return "call methodB";
    }

    @Override
    public String methodC() throws InterruptedException {
        int sleepTime = random.nextInt(100);
        Log.info("sleep {}ms", sleepTime);
        Thread.sleep(sleepTime);
        return "call methodC";
    }
}
