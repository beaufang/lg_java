package com.beau.service;

public interface ServercService {

    String methodA() throws InterruptedException;

    String methodB() throws InterruptedException;

    String methodC() throws InterruptedException;
}
