package com.beau;

import com.beau.service.CallService;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

public class ConsumerMain {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfig.class);
        context.start();
        CallService service = (CallService)context.getBean("callService");
        service.call();
        System.in.read();
    }

    @Configuration
    @EnableDubbo(scanBasePackages = {"com.beau.service"})
    @ComponentScan("com.beau.service")
    @PropertySource("classpath:dubbo.properties")
    static class ConsumerConfig {}
}
