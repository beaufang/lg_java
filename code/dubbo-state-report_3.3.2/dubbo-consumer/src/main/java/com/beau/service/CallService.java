package com.beau.service;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class CallService {

    @Reference
    private ServercService service;

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);

    /**
     * 按频率调用 dubbo 服务
     */
    public void call() {
        executor.scheduleAtFixedRate(() ->{
            try {
                String result = service.methodA();
//                System.out.println(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, 100L, 5, TimeUnit.MILLISECONDS);
        executor.scheduleAtFixedRate(() ->{
            try {
                String result = service.methodB();
//                System.out.println(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, 100L, 10, TimeUnit.MILLISECONDS);
        executor.scheduleAtFixedRate(() ->{
            try {
                String result = service.methodC();
//                System.out.println(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, 100L, 20, TimeUnit.MILLISECONDS);
    }
}
