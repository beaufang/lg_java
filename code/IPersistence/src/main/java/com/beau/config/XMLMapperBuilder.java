package com.beau.config;

import com.beau.pojo.Configuration;
import com.beau.pojo.MappedStatement;
import com.beau.sqlSession.SqlCommandType;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

public class XMLMapperBuilder {

    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration =configuration;
    }

    public void parse(InputStream inputStream) throws DocumentException {

        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();

        String namespace = rootElement.attributeValue("namespace");

        handMappedStatement(rootElement.selectNodes("//select"), namespace, SqlCommandType.SELECT);
        handMappedStatement(rootElement.selectNodes("//delete"), namespace, SqlCommandType.DELETE);
        handMappedStatement(rootElement.selectNodes("//update"), namespace, SqlCommandType.UPDATE);
        handMappedStatement(rootElement.selectNodes("//insert"), namespace, SqlCommandType.INSERT);

    }

    private void handMappedStatement(List<Element> list,String namespace,  SqlCommandType type) {
        for (Element element : list) {
            String id = element.attributeValue("id");
            String resultType = element.attributeValue("resultType");
            String parameterType = element.attributeValue("parameterType");
            String sqlText = element.getTextTrim();
            MappedStatement mappedStatement = new MappedStatement();
            mappedStatement.setId(id);
            mappedStatement.setResultType(resultType);
            mappedStatement.setParameterType(parameterType);
            mappedStatement.setSql(sqlText);
            mappedStatement.setSqlCommandType(type);
            String key = namespace+"."+id;
            configuration.getMappedStatementMap().put(key,mappedStatement);
        }
    }


}
