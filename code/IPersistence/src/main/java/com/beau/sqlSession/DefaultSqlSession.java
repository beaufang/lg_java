package com.beau.sqlSession;

import com.beau.pojo.Configuration;
import com.beau.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        Executor executor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return executor.query(configuration, mappedStatement, params);
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if (objects.size() == 1) {
            return (T) objects.get(0);
        }
        throw new RuntimeException("查询结果为空，或有多个");
    }

    @Override
    public int update(String statementId, Object... params) throws Exception {
        Executor executor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return executor.update(configuration, mappedStatement, params);
    }


    // 生成代理类
    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        Object proxyInstance = Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{mapperClass}, (proxy, method, args) -> {
            // 拼接 statementId
            String methodName = method.getName();
            String clazzName = method.getDeclaringClass().getName();
            String statementId = clazzName + "." + methodName;
            MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
            SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
            Object result = null;
            switch (sqlCommandType) {
                case UPDATE:
                    result = update(statementId, args);
                    break;
                case SELECT:
                    // 获取被调用方法的返回值类型
                    Type genericReturnType = method.getGenericReturnType();
                    // 判断是否进行了 泛型类型参数化
                    if (genericReturnType instanceof ParameterizedType) {
                        result = selectList(statementId, args);
                    }
                    result = selectOne(statementId, args);
                    break;
                case DELETE:
                    result = update(statementId, args);
                    break;
                case INSERT:
                    result = update(statementId, args);
                    break;

            }

            return result;

        });
        return (T) proxyInstance;
    }
}
