package com.beau.sqlSession;

import com.beau.pojo.Configuration;
import com.beau.pojo.MappedStatement;

import java.util.List;


public interface Executor {

    <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

    int update(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;


}
