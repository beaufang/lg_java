package com.beau.sqlSession;

import com.beau.config.XMLConfigBuilder;
import com.beau.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

public class SqlSessionFactoryBuilder {
    public SqlSessionFactory build(InputStream in) throws PropertyVetoException, DocumentException {
        // 解析配置文件，转换成 Configuration 对象
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(in);
        return new DefaultSqlSessionFactory(configuration);
    }
}
