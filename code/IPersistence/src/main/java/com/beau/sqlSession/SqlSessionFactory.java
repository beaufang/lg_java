package com.beau.sqlSession;

public interface SqlSessionFactory {
    SqlSession openSession();
}
