package com.example.controller;

import com.example.service.IHotPositionService;
import com.example.vo.HotPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/7/31
 */
@Controller
public class IndexController {

    @Autowired
    private IHotPositionService hotPositionService;

    @RequestMapping("positions")
    public String getHotPositions(Model model) {
        List<HotPosition> list = hotPositionService.findAllHotPosition();
        model.addAttribute("positions", list);
        return "index";
    }
}
