package com.example.listener;

import com.example.service.impl.GuavaCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
@Component
public class LocalCacheListener implements ApplicationListener<ApplicationStartedEvent> {

    @Autowired
    private GuavaCacheService guavaCacheService;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        guavaCacheService.loadHotPosition();
        System.out.println("guava 本地缓存初始化成功");
    }
}
