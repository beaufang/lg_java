package com.example.dao;

import com.example.vo.HotPosition;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
public interface HotPositionDao extends JpaRepository<HotPosition, Long> {

}
