package com.example.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
@Configuration
public class GuavaCacheConfig {

    @Bean
    public Cache<Object, Object> localCache(){
        return CacheBuilder.newBuilder().initialCapacity(10).maximumSize(20).build();
    }
}
