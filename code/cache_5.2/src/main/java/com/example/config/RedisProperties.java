package com.example.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
@Component
@ConfigurationProperties(prefix = "spring.redis")
public class RedisProperties {

    private String password;
    private cluster cluster;

    public static class cluster {
        private List<String> nodes;

        public List<String> getNodes() {
            return nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RedisProperties.cluster getCluster() {
        return cluster;
    }

    public void setCluster(RedisProperties.cluster cluster) {
        this.cluster = cluster;
    }
}
