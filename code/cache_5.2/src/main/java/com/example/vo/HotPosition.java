package com.example.vo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author BeauFang
 * Date: 2020/7/31
 */
@Data
@Entity
@Table(name = "t_position")
public class HotPosition implements Serializable {

    private static final long serialVersionUID = 4649059581447627962L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 职位
     */
    private String position;

    /**
     * 发布时间
     */
//    @Column(name = "publishTime")
    private Date publishTime;

    /**
     * 薪资
     */
    private Integer salary;

    /**
     * 工作经验
     */
    private String experience;

    /**
     * 学历
     */
    private String education;


    /**
     * 公司名称
     */
    private String company;

    /**
     * 岗位标签
     */
    private String tags;

    /**
     * 公司描述
     */
    private String description;

}
