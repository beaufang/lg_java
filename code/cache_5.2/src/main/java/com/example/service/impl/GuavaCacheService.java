package com.example.service.impl;

import com.example.vo.HotPosition;
import com.google.common.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
@Service
public class GuavaCacheService {

    private final String CACHE_HOT_POSITION_KEY = "lagou:hot-position";

    @Autowired
    private RedisService redisService;

    @Autowired
    private Cache<Object, Object> localCache;


    public void loadHotPosition() {
        List<HotPosition> hotPositions = redisService.getHotPosition();
        localCache.put(CACHE_HOT_POSITION_KEY, hotPositions);
    }

    @SuppressWarnings("unchecked")
    public List<HotPosition> getHotPositions() {
        return (List<HotPosition>)localCache.getIfPresent(CACHE_HOT_POSITION_KEY);
    }

}
