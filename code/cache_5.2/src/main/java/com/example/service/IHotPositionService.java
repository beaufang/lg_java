package com.example.service;

import com.example.vo.HotPosition;

import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
public interface IHotPositionService {

    /**
     * 查询所有热门职位
     * @return
     */
    List<HotPosition> findAllHotPosition();
}
