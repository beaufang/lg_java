package com.example.service.impl;

import com.example.vo.HotPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/8/1
 */
@Service
public class RedisService {

    private final String REDIS_HOT_POSITION_KEY = "lagou:hot-positions";

    @Autowired
    // 发现有一个 redisTemplate 和一个 stringRedisTemplate
    @Qualifier("redisTemplate")
    private RedisTemplate template;

    /**
     * 添加热门职位缓存
     * @param positionList 热门职位
     */
    public void setHotPosition(List<HotPosition> positionList) {
        BoundListOperations<String, HotPosition> ops = template.boundListOps(REDIS_HOT_POSITION_KEY);
        for (HotPosition p : positionList) {
            ops.leftPush(p);
        }
    }

    /**
     * 删除热门职位缓存
     */
    public void deleteHotPosition() {
        template.unlink(REDIS_HOT_POSITION_KEY);
    }

    /**
     * 获取所有热门职位
     * @return 热门职位
     */
    public List<HotPosition> getHotPosition() {
        return template.boundListOps(REDIS_HOT_POSITION_KEY).range(0, -1);
    }
}
