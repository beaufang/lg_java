package com.example.demo;

import com.example.dao.HotPositionDao;
import com.example.service.impl.RedisService;
import com.example.vo.HotPosition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
class CacheApplicationTests {

    @Autowired
    private HotPositionDao hotPositionDao;

    @Autowired
    private RedisService redisService;


    @Test
    void initData2SQL() {
        for (int i = 0; i < 9; i++) {
            HotPosition hotPosition = new HotPosition();
            hotPosition.setCompany("拉勾网络技术有限公司 " + i);
            hotPosition.setEducation("本科");
            hotPosition.setPublishTime(new Date());
            hotPosition.setSalary(15);
            hotPosition.setExperience("3-5年/不限");
            hotPosition.setPosition("Java 高级研发工程师");
            hotPosition.setTags("招聘,教育,互联网");
            hotPosition.setDescription("中国最大的招聘网站和教育网站之一");
            hotPositionDao.save(hotPosition);
        }
    }


    @Test
    void truncateSQLData() {
        hotPositionDao.deleteAll();
    }

    @Test
    void loadData2Redis() {
        List<HotPosition> positionList = hotPositionDao.findAll();
        redisService.setHotPosition(positionList);
    }

    @Test
    void deleteRedisCache() {
        redisService.deleteHotPosition();
    }

    @Test
    void printRedisCache() {
        List<HotPosition> hotPosition = redisService.getHotPosition();
        System.out.println(hotPosition);
    }



}
