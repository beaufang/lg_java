package com.beau.util;

import java.util.UUID;

public class TokenUtil {

    /**
     * 生成 token
     * @param origin
     * @return
     */
    public static String genToken(String origin) {
        return UUID.nameUUIDFromBytes(origin.getBytes()).toString();
    }

}
