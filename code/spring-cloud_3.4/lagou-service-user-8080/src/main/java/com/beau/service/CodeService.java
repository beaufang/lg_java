package com.beau.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "lagou-service-code")
@RequestMapping("/code")
public interface CodeService {

//    @GetMapping("/create/{email}")
//    Boolean create(@PathVariable(value = "email") String email);

    @GetMapping("/validate/{email}/{code}")
    Integer validate(@PathVariable(value = "email") String email,@PathVariable(value = "code") String code);
}
