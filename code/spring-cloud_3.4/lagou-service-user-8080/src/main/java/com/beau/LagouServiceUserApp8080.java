package com.beau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class LagouServiceUserApp8080 {

    public static void main(String[] args) {
        SpringApplication.run(LagouServiceUserApp8080.class, args);
    }
}
