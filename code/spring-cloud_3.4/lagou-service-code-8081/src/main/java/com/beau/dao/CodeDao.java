package com.beau.dao;

import com.beau.pojo.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * 验证码 Dao
 */
public interface CodeDao extends JpaRepository<Code, Integer>, JpaSpecificationExecutor<Code> {

    @Query("from Code where email=?1 and code=?2")
    Code findByEmailAndCode(String email, String code);
}
