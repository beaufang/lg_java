package com.beau.service.impl;

import com.beau.service.EmailService;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceFallback implements EmailService {
    @Override
    public Boolean sendEmail(String email, String code) {
        System.out.println("invoke fallback");
        return false;
    }
}
