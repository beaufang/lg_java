package com.beau.service;

import com.beau.pojo.Code;

import java.util.Date;

public interface CodeService {

    /**
     * 生成验证码
     * @return
     */
    String genCode();

    Code saveCode(String code, String email, Date createTime, Date expireTime);

    Integer validateCode(String email, String code);
}
