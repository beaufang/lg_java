package com.beau.service;

import com.beau.service.impl.EmailServiceFallback;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "lagou-service-email", path = "/email", fallback = EmailServiceFallback.class)
public interface EmailService {

    @GetMapping("/{email}/{code}")
    Boolean sendEmail(@PathVariable("email") String  email, @PathVariable("code") String code);
}
