package com.beau.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "lagou-service-user", path = "/user")
public interface UserService {

    @GetMapping("/info/{token}")
    String info(@PathVariable("token") String token);
}
