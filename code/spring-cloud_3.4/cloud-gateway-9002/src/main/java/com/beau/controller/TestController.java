package com.beau.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试用
 */
@RestController
@RefreshScope
public class TestController {

    @Value("${test.word}")
    private String word;

    @GetMapping("/test")
    public String test() {
        return word;
    }
}
