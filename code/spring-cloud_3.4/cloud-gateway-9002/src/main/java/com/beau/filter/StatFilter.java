package com.beau.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 与作业无关，只是打印请求记录
 */
@Component
public class StatFilter implements GlobalFilter, Ordered {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("request uri: " + exchange.getRequest().getURI().toString());
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -10;
    }
}
