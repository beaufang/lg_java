package com.beau.controller;

import com.beau.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class EmailController {

    private final Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    /**
     * 发送验证码
     * @param email
     * @param code
     * @return
     */
    @GetMapping("/{email}/{code}")
    public Boolean sendEmail(@PathVariable("email") String email, @PathVariable("code") String code) {
        logger.info("email is " + email + " code is " + code);
        Boolean result = emailService.sendEmail(email, code, "验证码");
//        try {
//            Thread.sleep(1000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return result;
    }
}
