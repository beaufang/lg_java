package com.beau.service;

public interface EmailService {
    Boolean sendEmail(String to, String content, String subject);
}
