package com.beau.service.impl;

import com.beau.service.WelcomeService;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.RpcContext;

@Service
public class WelcomeServiceImpl implements WelcomeService {
    @Override
    public String welCome(String name) {
        System.out.println("consumer ip is " + RpcContext.getContext().getAttachment("consumer_ip"));
        return "welcome to " + name;
    }
}
