package com.example.demo.controller;

import com.beau.service.HelloService;
import com.beau.service.WelcomeService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @Reference
    private HelloService helloService;

    @Reference
    private WelcomeService welcomeService;

    @RequestMapping("/")
    public String index() {
        return "visit serverA or serverB";
    }

    @RequestMapping("/serverA")
    public String serverA() {
        return helloService.sayHello("serverA");
    }

    @RequestMapping("/serverB")
    public String serverB() {
        return welcomeService.welCome("serverB");
    }
}
