package com.beau;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class ServerAMain {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-*.xml");
        context.start();
        System.in.read();
    }
}
