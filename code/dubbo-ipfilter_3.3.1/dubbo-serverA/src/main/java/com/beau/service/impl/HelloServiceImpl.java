package com.beau.service.impl;

import com.beau.service.HelloService;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.RpcContext;

@Service
public class HelloServiceImpl implements HelloService {
    public String sayHello(String name) {
        System.out.println("consumer ip is " + RpcContext.getContext().getAttachment("consumer_ip"));
        return "hello " + name;
    }
}
