package com.beau.service;

public interface HelloService {
    String sayHello(String name);
}
