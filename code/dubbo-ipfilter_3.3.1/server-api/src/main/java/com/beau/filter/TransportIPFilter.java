package com.beau.filter;


import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Activate(group = CommonConstants.CONSUMER)
public class TransportIPFilter implements Filter {
    private static final Logger Log = LoggerFactory.getLogger(TransportIPFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        try {
            invocation.setAttachment("consumer_ip", InetAddress.getLocalHost().getHostAddress());
            Log.info("fill consumer ip");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return invoker.invoke(invocation);
    }
}
