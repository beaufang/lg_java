package com.example;

import com.example.coder.JSONSerializer;
import com.example.coder.RpcEncoder;
import com.example.container.ServiceCentre;
import com.example.request.RpcRequest;
import com.example.service.UserService;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;

import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class RpcClient {

    private String host;
    private int port;
    //创建线程池对象
    private ExecutorService executor;
    private RpcHandler rpcHandler;
    private EventLoopGroup group;

    public RpcClient(String host, int port) {
        this.host = host;
        this.port = port;
        this.executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        init();
    }


    /**
     * 生成代理对象
     *
     * @param clz
     * @return
     */
    public Object createProxy(Class clz) {
        Class[] cls = new Class[]{clz};
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), cls, (proxy, method, args) -> {
            // 封装 rpc request
            RpcRequest rpcRequest = new RpcRequest();
            rpcRequest.setClassName(clz.getName());
            rpcRequest.setMethodName(method.getName());
            rpcRequest.setParameters(args);
            // 填充参数类型
            Class[] parameterTypes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                parameterTypes[i] = args[i].getClass();
            }
            rpcRequest.setParameterTypes(parameterTypes);
            // 提交请求
            rpcHandler.setRequest(rpcRequest);
            return executor.submit(rpcHandler).get();
        });
    }


    /**
     * 初始化
     */
    public void init() {
        group = new NioEventLoopGroup();
        rpcHandler = new RpcHandler();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(rpcHandler);
                    }
                });

        try {
            System.out.println("rpc client connect to " + host + ":" + port);
            bootstrap.connect(host, port).sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭服务
     */
    public void shutDown() {
        executor.shutdown();
        group.shutdownGracefully(0, 10, TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) throws Exception {
        ServiceCentre centre = new ServiceCentre("127.0.0.1:2181");
        centre.subscribe(RpcClient::new, (Conn) -> {
            ((RpcClient)Conn).shutDown();
        });

        // 执行调用
        while (true) {
            RpcClient rpcClient = (RpcClient) centre.getConnection();
            UserService proxy = (UserService) rpcClient.createProxy(UserService.class);
            String result = proxy.sayHello("are you ok?" + " server " + rpcClient.host + ":" + rpcClient.port);
            System.out.println(new Date().toString() + " " + result);
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++ \r\n");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
