package com.example.container;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

import java.nio.charset.StandardCharsets;

public class RegisterService {

    public static void register(String zkUrl, String serverUrl, String serverName) throws Exception {
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString(zkUrl)
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(6000)
                .connectionTimeoutMs(3000)
                .namespace("services")
                .build();
        client.start();
        client.create().withMode(CreateMode.EPHEMERAL).forPath("/" + serverName, serverUrl.getBytes(StandardCharsets.UTF_8));
    }
}
