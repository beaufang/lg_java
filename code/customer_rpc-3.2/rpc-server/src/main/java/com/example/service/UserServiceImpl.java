package com.example.service;

import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public String sayHello(String word) {
        System.out.println(word);
        return "success";
    }

}
