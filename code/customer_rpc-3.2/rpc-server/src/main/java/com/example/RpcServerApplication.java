package com.example;

import com.example.container.RegisterService;
import com.example.server.RpcServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RpcServerApplication {
//	@Value("${rpc.port}")
//	private int port;
//	@Value("${rpc.host}")
//	private String host;

	public static void main(String[] args) {
		SpringApplication.run(RpcServerApplication.class, args);
	}

	@Bean(initMethod = "start")
	public RpcServer rpcServer() throws Exception {
		String host = "127.0.0.1";
		int port = Integer.parseInt(System.getProperty("port"));
		RpcServer rpcServer = new RpcServer(host, port);
		String serverName = System.getProperty("serverName");
		RegisterService.register("127.0.0.1:2181", host + ":" + port, serverName);
		return rpcServer;
	}

}
