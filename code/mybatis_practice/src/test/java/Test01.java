import com.beau.mapper.IOrderMapper;
import com.beau.mapper.IUserMapper;
import com.beau.pojo.Orders;
import com.beau.pojo.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test01 {
    private SqlSession sqlSession;
    private IUserMapper userMapper;

    @Before
    public void init() throws IOException {
        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(IUserMapper.class);
    }

    @Test
    public void testAddUser() {
        User user = new User();
        user.setUsername("lisi");
        userMapper.insert(user);
        sqlSession.close();
    }

    @Test
    public void testPageHelper() {
        PageHelper.startPage(1,1);
        Example example = new Example(User.class);
        List<User> users = userMapper.selectByExample(example);
        PageInfo<User> pageInfo = new PageInfo<User>(users);
        System.out.println(pageInfo);
    }

    @Test
    public void testFindOrderAndUser() {
        IOrderMapper mapper = sqlSession.getMapper(IOrderMapper.class);
        List<Orders> list = mapper.findOrderAndUser();
        for (Orders o : list) {
            System.out.println(o);
        }
    }

    @Test
    public void testFindUserAndOrders() {
        List<User> list = userMapper.findUserAndOrder();
        for (User u : list) {
            System.out.println(u);
        }
    }
}
