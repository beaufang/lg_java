package com.beau.mapper;

import com.beau.pojo.Orders;
import com.beau.pojo.User;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface IUserMapper extends Mapper<User> {

    @Select("select * from user where id=#{id}")
    User findById(int id);

    @Select("select * from user")
    @Results({@Result(id = true, property = "id", column = "id")
            ,@Result(property = "ordersList", column = "id", javaType = List.class,
                many = @Many(select = "com.beau.mapper.IOrderMapper.findOrderByUid"))})
    List<User> findUserAndOrder();
}