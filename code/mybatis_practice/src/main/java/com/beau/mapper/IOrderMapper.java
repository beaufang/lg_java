package com.beau.mapper;

import com.beau.pojo.Orders;
import com.beau.pojo.User;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface IOrderMapper extends Mapper<Orders> {

    @Select("select * from orders")
    @Results({
            @Result(property = "user", column = "uid", javaType = User.class,
                    one = @One(select = "com.beau.mapper.IUserMapper.findById"))
    })
    List<Orders> findOrderAndUser();

    @Select("select * from orders where uid = #{uid}")
    List<Orders> findOrderByUid(int uid);
}
