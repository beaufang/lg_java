package com.beau;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.data.Stat;

/**
 * 更新 zk 中数据库连接配置信息
 */
public class ConfigPub {

    public static void main(String[] args) throws Exception {
        String zkUrl = "127.0.0.1:2181";
        String url= "jdbc:mysql://localhost:3306/blog_system?serverTimezone=UTC";
//        String url= "jdbc:mysql://localhost:3306/blog_system2?serverTimezone=UTC";
        String username= "root";
        String password = "Test_123";
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString(zkUrl)
                .retryPolicy(retryPolicy)
                .namespace("config-center")
                .build();
        client.start();
        Stat stat = client.checkExists().forPath("/datasource");
        if (stat == null) {
            client.create().forPath("/datasource");
        }
        String data = url + ";" + username + ";" + password;
        client.setData().forPath("/datasource", data.getBytes("UTF-8"));
        client.close();
    }

}
