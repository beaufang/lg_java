package com.example.service.impl;


import com.example.pojo.Position;
import com.example.service.PositionService;
import org.apache.commons.beanutils.BeanUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author BeauFang
 * Date: 2020/9/22
 */
@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    RestHighLevelClient esClient;

    @Override
    public List<Position> queryPositions(String query) throws IOException {
        // 第一次查询
        SearchRequest req1 = new SearchRequest("lagou_position");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("positionName", query));
        req1.source(sourceBuilder);
        SearchResponse rep1 = esClient.search(req1, RequestOptions.DEFAULT);
        SearchHit[] hits = rep1.getHits().getHits();
        System.out.println("查询到的职位数量" + hits.length);
        List<Position> list = new ArrayList<>();
        for (SearchHit hit : hits) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            Position p = new Position();
            try {
                BeanUtils.populate(p, sourceAsMap);
                list.add(p);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        // 第二次查询
        if (list.size() < 5) {
            SearchRequest req2 = new SearchRequest("lagou_position");
            SearchSourceBuilder sourceBuilder2 = new SearchSourceBuilder();
            sourceBuilder2.query(QueryBuilders.queryStringQuery("美女多 OR 员工福利好").defaultField("positionAdvantage"));
            req2.source(sourceBuilder2);
            SearchResponse rep2 = esClient.search(req2, RequestOptions.DEFAULT);
            SearchHit[] hits2 = rep2.getHits().getHits();
            System.out.println("补充查询到职位数量为" + hits2.length);
            for (SearchHit hit : hits2) {
                Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                Position p = new Position();
                try {
                    BeanUtils.populate(p, sourceAsMap);
                    list.add(p);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
                if (list.size() > 5) {
                    break;
                }
            }
        }
        return list;
    }
}
