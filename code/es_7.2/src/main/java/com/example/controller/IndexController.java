package com.example.controller;

import com.example.pojo.Position;
import com.example.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

/**
 * @author BeauFang
 * Date: 2020/9/23
 */
@Controller
public class IndexController {

    @Autowired
    PositionService positionService;

    @RequestMapping("/getPositions")
    public String queryPositions(String positionName, Model model) throws IOException{
        List<Position> positions = positionService.queryPositions(positionName);
        model.addAttribute("positions", positions);
        return "index";
    }
}
