package com.example.pojo;

import lombok.Data;

/**
 * @author BeauFang
 * Date: 2020/9/22
 */
@Data
public class Position {
    private String id;
    private String companyName;
    private String positionAdvantage;
    private String positionName;
    private String salary;
    private String salaryMin;
    private String salaryMax;
    private String education;
    private String workYear;
    private String jobNature;
    private String email;
    private String city;
    private String workAddress;
    private String forwardEmail;
}
