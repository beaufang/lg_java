<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SpringMVC 测试页</title>


    <script type="text/javascript" src="/js/jquery.min.js"></script>

    <script>

        function login() {
            const username = $("#username").val();
            const pass = $("#password").val();

            if (username.value === "") {
                alert("请输入用户名");
            } else if (pass.value  === "") {
                alert("请输入密码");
            } else {
                $.ajax({
                    url: '/login',
                    type: 'POST',
                    data: JSON.stringify({"username":username,"password": pass}),
                    contentType: 'application/json;charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 200) {
                            window.location = "/html/detail.html";
                        } else {
                            alert(data.data);
                        }
                    }
                })
            }
        }
    </script>


    <style>
        body {
            background-color: cornsilk;
            background-size: 100%;
            background-repeat: no-repeat;
        }

        #login_frame {
            width: 400px;
            height: 260px;
            padding: 13px;

            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -200px;
            margin-top: -200px;

            background-color: rgba(240, 255, 255, 0.5);

            border-radius: 10px;
            text-align: center;
        }

        form p > * {
            display: inline-block;
            vertical-align: middle;
        }

        #image_logo {
            margin-top: 22px;
        }

        .label_input {
            font-size: 14px;
            font-family: 宋体;

            width: 65px;
            height: 28px;
            line-height: 28px;
            text-align: center;

            color: white;
            background-color: #3CD8FF;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .text_field {
            width: 278px;
            height: 28px;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            border: 0;
        }

        #btn_login {
            font-size: 14px;
            font-family: 宋体;

            width: 120px;
            height: 28px;
            line-height: 28px;
            text-align: center;

            color: white;
            background-color: #3BD9FF;
            border-radius: 6px;
            border: 0;

            float: left;
        }

        #forget_pwd {
            font-size: 12px;
            color: white;
            text-decoration: none;
            position: relative;
            float: right;
            top: 5px;

        }

        #forget_pwd:hover {
            color: blue;
            text-decoration: underline;
        }

        #login_control {
            padding: 0 28px;
        }
    </style>
</head>
<body>

<div id="login_frame">

    <p><label style="font-size: 24px; color: #3BD9FF">测试登录</label></p>
    <form method="post">

        <p><label class="label_input">用户名</label><input type="text" id="username" class="text_field"/></p>
        <p><label class="label_input">密码</label><input type="text" id="password" class="text_field"/></p>

        <div id="login_control">
            <input type="button" id="btn_login" onclick="login()" value="登录"/>
        </div>
    </form>
</div>

</body>
</html>
