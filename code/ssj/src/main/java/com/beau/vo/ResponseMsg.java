package com.beau.vo;

public class ResponseMsg {

    private Object data;
    private int status;

    public ResponseMsg(Object data, int status) {
        this.data = data;
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
