package com.beau.controller;

import com.beau.pojo.Resume;
import com.beau.service.IResumeService;
import com.beau.vo.ResponseMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/resume")
public class ResumeController {

    @Autowired
    private IResumeService resumeService;

    @RequestMapping("/detail")
    @ResponseBody
    public ResponseMsg detail() {
        return new ResponseMsg(resumeService.findAll(), 200);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseMsg delete(@RequestBody Resume resume) {
        System.out.println("delete");
        resumeService.delete(resume);
        return new ResponseMsg("ok", 200);
    }

    @RequestMapping("/update")
    @ResponseBody
    public ResponseMsg update(@RequestBody Resume resume) {
        System.out.println("update");
        resumeService.update(resume);
        return new ResponseMsg("ok", 200);
    }

    @RequestMapping("/insert")
    @ResponseBody
    public ResponseMsg insert(@RequestBody Resume resume) {
        System.out.println("insert");
        resumeService.insert(resume);
        return new ResponseMsg("ok", 200);
    }
}
