package com.beau;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * @author BeauFang
 * Date: 2020/7/21
 */
public class RedisClusterTest {

    public static void main(String[] args) {
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(new HostAndPort("192.168.158.124", 7001));
        nodes.add(new HostAndPort("192.168.158.124", 7002));
        nodes.add(new HostAndPort("192.168.158.124", 7003));
        nodes.add(new HostAndPort("192.168.158.124", 7004));
        nodes.add(new HostAndPort("192.168.158.124", 7021));
        nodes.add(new HostAndPort("192.168.158.124", 7022));
        nodes.add(new HostAndPort("192.168.158.124", 7023));
        nodes.add(new HostAndPort("192.168.158.124", 7024));
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisCluster jedisCluster = new JedisCluster(nodes, jedisPoolConfig);
        jedisCluster.set("name1", "zhangfei");
        jedisCluster.set("name2", "zhaoyun");
        System.out.println(jedisCluster.get("name1"));
        System.out.println(jedisCluster.get("name2"));
    }
}
