package com.lagou.edu.mvcframework.pojo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * 封装handler方法相关的信息
 */
public class Handler {

    private Object controller; // method.invoke(obj,)

    private Method method;

    private Pattern pattern; // spring中url是支持正则的

    private PermissionCtl permissionCtl; // 用于控制访问权限

    private Map<String,Integer> paramIndexMapping; // 参数顺序,是为了进行参数绑定，key是参数名，value代表是第几个参数 <name,2>


    public Handler(Object controller, Method method, Pattern pattern) {
        this(controller, method, pattern, new PermissionCtl(null, false));
//        this.controller = controller;
//        this.method = method;
//        this.pattern = pattern;
//        this.paramIndexMapping = new HashMap<>();
//        this.permissionCtl = new PermissionCtl(null, false) ; // 不做权限控制
    }

    public Handler(Object controller, Method method, Pattern pattern, PermissionCtl permissionCtl) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMapping = new HashMap<>();
        this.permissionCtl = permissionCtl;
    }

    public PermissionCtl getPermissionCtl() {
        return permissionCtl;
    }

    public void setPermissionCtl(PermissionCtl permissionCtl) {
        this.permissionCtl = permissionCtl;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Map<String, Integer> getParamIndexMapping() {
        return paramIndexMapping;
    }

    public void setParamIndexMapping(Map<String, Integer> paramIndexMapping) {
        this.paramIndexMapping = paramIndexMapping;
    }
}
