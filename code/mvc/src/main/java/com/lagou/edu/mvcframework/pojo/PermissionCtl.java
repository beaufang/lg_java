package com.lagou.edu.mvcframework.pojo;

import java.util.regex.Pattern;

public class PermissionCtl {

    private Pattern pattern; // 为 null 表示通过所有
    private boolean forbidAll; // 是否禁止所有人访问

    public PermissionCtl(Pattern pattern, boolean forbidAll) {
        this.pattern = pattern;
        this.forbidAll = forbidAll;
    }

    // 权限校验
    public boolean validate(String in) {
        if (forbidAll) {
            return false;
        }else if (pattern == null) {
            return true;
        } else {
            return pattern.matcher(in).matches();
        }
    }

}
