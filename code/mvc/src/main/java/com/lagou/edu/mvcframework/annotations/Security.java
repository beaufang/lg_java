package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    /**
     * 规则：
     *  1. null 表示未使用该注解，任何人都可以访问
     *  2. {} 空数组，表示任何人都不能访问
     *  3. {"zhangsan", "lisi"} 表示里面的人都可以访问
     */
    String[] value() default {};
}
