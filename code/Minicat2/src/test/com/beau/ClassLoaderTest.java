package com.beau;

import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoaderTest {

    @Test
    public void testLoadClass() throws ClassNotFoundException, MalformedURLException, IllegalAccessException, InstantiationException {
        File file = new File("D:\\mytmp\\lg\\course\\lg_java\\code\\Minicat\\webapps\\demo1");
        URL url = file.toURL();
        System.out.println(url.toString());
        Class<?> aClass = new URLClassLoader(new URL[]{url}).loadClass("server.LagouServlet");
        Object instance = aClass.newInstance();
        System.out.println(aClass);
        System.out.println(instance);
    }
}
