package server;

import java.util.HashMap;
import java.util.Map;

public class Mapper {

    private Host host;

    public Mapper() {
    }


    public void setHost(Host host) {
        this.host = host;
    }

    public Host getHost() {
        return this.host;
    }

    public void addWrapper(Wrapper wrapper) {
        this.host.context.addWrapper(wrapper);
    }

    public String getAppBase() {
        return this.host.context.appBase;
    }

    public Wrapper getWrapper(String url) {
        return this.host.context.getWrapper(url);
    }



    static class Host {
        String hostName;
        Context context;

        public Host(String hostName, Context context) {
            this.hostName = hostName;
            this.context = context;
        }
    }

    static class Context {
       String appBase; // appBase 目录
       Map<String, Wrapper> map = new HashMap<>();

        public Context(String appBase) {
            this.appBase = appBase;
        }

        public void addWrapper(Wrapper wrapper) {
            map.put(wrapper.url, wrapper);
        }

        public Wrapper getWrapper(String url) {
            return this.map.get(url);
        }
    }

    static class Wrapper {
       String url; // 访问的 url
       Servlet servlet;

        public Wrapper(String url, Servlet servlet) {
            this.url = url;
            this.servlet = servlet;
        }
    }


}
