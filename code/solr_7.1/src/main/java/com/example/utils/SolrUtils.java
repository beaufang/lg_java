package com.example.utils;

import com.alibaba.fastjson.JSON;
import org.apache.solr.common.SolrDocument;

/**
 * @author BeauFang
 * Date: 2020/9/23
 */
public class SolrUtils {

    public static <T> T solrDocument2Bean(SolrDocument document, Class<T> clazz) {
        String documentJson = JSON.toJSONString(document);
        return JSON.parseObject(documentJson, clazz);
    }
}
