package com.example.service;

import com.example.pojo.Position;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;


/**
 * @author BeauFang
 * Date: 2020/9/22
 */
public interface PositionService {

    List<Position> queryPositions(String query) throws IOException, SolrServerException;
}
