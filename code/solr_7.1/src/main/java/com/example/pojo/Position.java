package com.example.pojo;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;

/**
 * @author BeauFang
 * Date: 2020/9/22
 */
@Data
public class Position {
    @Field
    private String id;
    @Field
    private String companyName;
    @Field
    private String positionAdvantage;
    @Field
    private String positionName;
    @Field
    private String salary;
    @Field
    private String salaryMin;
    @Field
    private String salaryMax;
    @Field
    private String education;
    @Field
    private String workYear;
    @Field
    private String jobNature;
    @Field
    private String email;
    @Field
    private String city;
    @Field
    private String workAddress;
    @Field
    private String forwardEmail;
}
