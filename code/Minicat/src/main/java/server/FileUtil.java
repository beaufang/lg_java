package server;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

    /**
     * 获取目录下所有的文件夹名称
     * @param path 目录路劲
     * @return
     */
    public static List<String> listDir(String path) {
        List<String> list = new ArrayList<>();
        File file = new File(path);
        File[] files = file.listFiles(File::isDirectory);
        for (File f : files) {
            list.add(f.getName());
        }
        return list;
    }
}
