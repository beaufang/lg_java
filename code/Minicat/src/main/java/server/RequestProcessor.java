package server;

import java.io.InputStream;
import java.net.Socket;

public class RequestProcessor extends Thread {

    private Socket socket;
    private  Mapper mapper;

    public RequestProcessor(Socket socket, Mapper mapper){
        this.socket = socket;
        this.mapper = mapper;
    }

    @Override
    public void run() {
        try{
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理
            Mapper.Wrapper wrapper = mapper.find(request.getUrl());
            if( wrapper == null) {
                response.outputHtml(mapper.findStatic(request.getUrl()));
            }else{
                // 动态资源servlet请求
                HttpServlet httpServlet = (HttpServlet) wrapper.servlet;
                httpServlet.service(request,response);
            }
            System.out.println("请求处理完毕...");
            inputStream.close();
            socket.close();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}
